import os
import os.path
from db_helper import DatabaseHelper

DATABASE_NAME = "todo.db"

def menu(db):
    tasks = db.get_tasks()
    print("Tasks:")
    for task in tasks:
        print(f"\t{task[0]}) {task[3]} - {task[4]}")
    print("")
    print("Options:")
    print("\t1) Add Task")
    print("\t2) Complete Task")
    print("\t3) List Completed Tasks")
    print("\t4) WIP: Delete Tasks")
    return input("Selection, `q` to quit > ")

def clear():
    if(os.name == 'posix'):
        os.system('clear')
    else:
        os.system('cls')

if __name__ == '__main__':
    db = DatabaseHelper(DATABASE_NAME)

    while True:
        clear()
        option = menu(db)
        if option == "q":
            break
        elif int(option) == 1:
            # Add task
            name = input("Input Task Title > ")
            description = input("Input Task Description > ")
            db.add_task(name, description)
        elif int(option) == 2:
            # Complete task
            task_id = input("Input Task ID > ")
            db.complete_task(int(task_id))
        elif int(option) == 3:
            # List completed tasks
            clear()
            print("Tasks:")
            tasks = db.get_completed_tasks()
            for task in tasks:
                print(f"\t{task[0]}) {task[3]} - {task[4]}")
            os.system('pause')
        elif int(option) == 3:
            # Delete tasks
            pass
