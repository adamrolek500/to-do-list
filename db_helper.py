from datetime import datetime
import os
import sqlite3


class DatabaseHelper:
    def __init__(self, database_file):
        if not os.path.isfile(database_file):
            print("Did not find DB. Preforming first time setup.")
            self.first_time_setup(database_file)
        else:
            print("found DB")
            self.connect(database_file)

    def complete_task(self, task_id):
        insert = "UPDATE Tasks SET date_completed = ? WHERE task_id = ?;"
        data = (datetime.now(), task_id)
        cur = self.connection.cursor()
        cur.execute(insert, data)
        self.connection.commit()

    def add_task(self, task_name, task_description):
        data = (datetime.now(), None, task_name, task_description)
        insert = "INSERT INTO Tasks (date_added, date_completed, task_name, task_description) VALUES (?, ?, ?, ?);"
        self.connection.execute(insert, data)
        self.connection.commit()

    def get_tasks(self):
        cur = self.connection.cursor()
        cur.execute(f"SELECT * FROM Tasks WHERE date_completed IS NULL ORDER BY task_id;")
        return cur.fetchall()
    
    def get_completed_tasks(self):
        cur = self.connection.cursor()
        cur.execute(f"SELECT * FROM Tasks WHERE date_completed IS NOT NULL ORDER BY task_id;")
        return cur.fetchall()

    def connect(self, database_file):
        try:
            self.connection = sqlite3.connect(database_file)
        except Exception as e:
            raise e

    def first_time_setup(self, database_file):
        self.connect(database_file)
        self.connection.execute(
            """CREATE TABLE Tasks(
                task_id INTEGER PRIMARY KEY, 
                date_added text,
                date_completed text,
                task_name varchar(50),
                task_description varchar(255)
                );"""
        )
        self.connection.commit()